
package org.enterpriseflowsrepository.api.transcodifications.quarkus.beans;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


/**
 * Root Type for DataSource
 * <p>
 * Source of data
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "name",
    "description",
    "indexes"
})
public class DataSource {

    @JsonProperty("name")
    private String name;
    @JsonProperty("description")
    private String description;
    /**
     * 
     * 
     */
    @JsonProperty("indexes")
    @JsonPropertyDescription("")
    private List<Element> indexes = new ArrayList<Element>();

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 
     * 
     */
    @JsonProperty("indexes")
    public List<Element> getIndexes() {
        return indexes;
    }

    /**
     * 
     * 
     */
    @JsonProperty("indexes")
    public void setIndexes(List<Element> indexes) {
        this.indexes = indexes;
    }

}
