package org.enterpriseflowsrepository.api.transcodifications.quarkus;

import java.lang.Object;
import java.lang.String;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import org.enterpriseflowsrepository.api.transcodifications.quarkus.beans.DataSource;
import org.enterpriseflowsrepository.api.transcodifications.quarkus.beans.Index;
import org.enterpriseflowsrepository.api.transcodifications.quarkus.beans.Transcodification;

/**
 * A JAX-RS interface.  An implementation of this interface must be provided.
 */
@Path("/datasource")
public interface DatasourceResource {
  /**
   * Gets a list of all `DataSource` entities.
   */
  @GET
  @Produces("application/json")
  List<DataSource> getdatasources();

  /**
   * Creates a new instance of a `DataSource`.
   */
  @POST
  @Consumes("application/json")
  void createDataSource(DataSource data);

  /**
   * Gets the details of a single instance of a `DataSource`.
   */
  @Path("/{datasourceId}")
  @GET
  @Produces("application/json")
  DataSource getDataSource(@PathParam("datasourceId") String datasourceId);

  /**
   * Updates an existing `DataSource`.
   */
  @Path("/{datasourceId}")
  @PUT
  @Consumes("application/json")
  void updateDataSource(@PathParam("datasourceId") String datasourceId, DataSource data);

  /**
   * Deletes an existing `DataSource`.
   */
  @Path("/{datasourceId}")
  @DELETE
  void deleteDataSource(@PathParam("datasourceId") String datasourceId);

  /**
   * Gets a list of all `Index` entities.
   */
  @Path("/{datasourceId}/index")
  @GET
  @Produces("application/json")
  List<Index> getindices(@PathParam("datasourceId") Object datasourceId);

  /**
   * Creates a new instance of a `Index`.
   */
  @Path("/{datasourceId}/index")
  @POST
  @Consumes("application/json")
  void createIndex(@PathParam("datasourceId") Object datasourceId, Index data);

  /**
   * Gets the details of a single instance of a `Index`.
   */
  @Path("/{datasourceId}/index/{indexId}")
  @GET
  @Produces("application/json")
  Index getIndex(@PathParam("datasourceId") String datasourceId,
      @PathParam("indexId") Object indexId);

  /**
   * Updates an existing `Index`.
   */
  @Path("/{datasourceId}/index/{indexId}")
  @PUT
  @Consumes("application/json")
  void updateIndex(@PathParam("datasourceId") String datasourceId,
      @PathParam("indexId") Object indexId, Index data);

  /**
   * Deletes an existing `Index`.
   */
  @Path("/{datasourceId}/index/{indexId}")
  @DELETE
  void deleteIndex(@PathParam("datasourceId") String datasourceId,
      @PathParam("indexId") Object indexId);

  /**
   * Gets a list of all `Transcodification` entities.
   */
  @Path("/{datasourceId}/index/{indexId}/transcodification")
  @GET
  @Produces("application/json")
  List<Transcodification> gettranscodifications(@PathParam("datasourceId") Object datasourceId,
      @PathParam("indexId") Object indexId, @HeaderParam("key") String key,
      @HeaderParam("to") String to);

  /**
   * Creates a new instance of a `Transcodification`.
   */
  @Path("/{datasourceId}/index/{indexId}/transcodification")
  @POST
  @Consumes("application/json")
  void createTranscodification(@PathParam("datasourceId") Object datasourceId,
      @PathParam("indexId") Object indexId, @HeaderParam("key") String key,
      @HeaderParam("to") String to, Transcodification data);

  /**
   * Gets the details of a single instance of a `Transcodification`.
   */
  @Path("/{datasourceId}/index/{indexId}/transcodification/{transcodificationId}")
  @GET
  @Produces("application/json")
  Transcodification getTranscodification(@PathParam("datasourceId") String datasourceId,
      @PathParam("indexId") Object indexId,
      @PathParam("transcodificationId") Object transcodificationId);

  /**
   * Updates an existing `Transcodification`.
   */
  @Path("/{datasourceId}/index/{indexId}/transcodification/{transcodificationId}")
  @PUT
  @Consumes("application/json")
  void updateTranscodification(@PathParam("datasourceId") String datasourceId,
      @PathParam("indexId") Object indexId,
      @PathParam("transcodificationId") Object transcodificationId, Transcodification data);

  /**
   * Deletes an existing `Transcodification`.
   */
  @Path("/{datasourceId}/index/{indexId}/transcodification/{transcodificationId}")
  @DELETE
  void deleteTranscodification(@PathParam("datasourceId") String datasourceId,
      @PathParam("indexId") Object indexId,
      @PathParam("transcodificationId") Object transcodificationId);
}
